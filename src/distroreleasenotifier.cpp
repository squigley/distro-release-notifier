/*
    SPDX-FileCopyrightText: 2018 Jonathan Riddell <jr@jriddell.org>
    SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>
    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "distroreleasenotifier.h"

#include <NetworkManagerQt/Manager>
#include <KOSRelease>

#include <QDate>
#include <QFile>
#include <QJsonDocument>
#include <QProcess>
#include <QStandardPaths>
#include <QTimer>
#include <QVersionNumber>

#include "config.h"
#include "dbusinterface.h"
#include "debug.h"
#include "notifier.h"
#include "upgraderprocess.h"

DistroReleaseNotifier::DistroReleaseNotifier(QObject *parent)
    : QObject(parent)
    , m_dbus(new DBusInterface(this))
    , m_checkerProcess(nullptr)
    , m_notifier(new Notifier(this))
    , m_hasChecked(false)
{
    // check after 10 seconds
    auto networkTimer = new QTimer(this);
    networkTimer->setSingleShot(true);
    networkTimer->setInterval(10 * 1000);
    connect(networkTimer, &QTimer::timeout, this, &DistroReleaseNotifier::releaseUpgradeCheck);
    networkTimer->start();

    auto dailyTimer = new QTimer(this);
    dailyTimer->setInterval(24 * 60 * 60 * 1000); // refresh once every day
    connect(dailyTimer, &QTimer::timeout,
            this, &DistroReleaseNotifier::forceCheck);
    dailyTimer->start();

    auto networkNotifier = NetworkManager::notifier();
    connect(networkNotifier, &NetworkManager::Notifier::connectivityChanged,
            this, [networkTimer](NetworkManager::Connectivity connectivity) {
        if (connectivity == NetworkManager::Connectivity::Full) {
            // (re)start the timer. The timer will make sure we collect up
            // multiple signals arriving in quick succession into a single
            // check.
            networkTimer->start();
        }
    });

    connect(m_dbus, &DBusInterface::useDevelChanged,
            this, &DistroReleaseNotifier::forceCheck);
    connect(m_dbus, &DBusInterface::pollingRequested,
            this, &DistroReleaseNotifier::forceCheck);

    connect(m_notifier, &Notifier::activateRequested,
            this, &DistroReleaseNotifier::releaseUpgradeActivated);
}

void DistroReleaseNotifier::releaseUpgradeCheck()
{
    if (m_hasChecked) {
        // Don't check again if we had a successful check again. We don't wanna
        // be spamming the user with the notification. This is reset eventually
        // by a timer to remind the user.
        return;
    }

    const QString checkerFile =
            QStandardPaths::locate(QStandardPaths::GenericDataLocation,
                                   QStringLiteral("distro-release-notifier/releasechecker"));
    if (checkerFile.isEmpty()) {
        qCWarning(NOTIFIER) << "Couldn't find the releasechecker"
                            << checkerFile
                            << QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
        return;
    }

    if (m_checkerProcess) {
        // Guard against multiple polls from dbus
        qCDebug(NOTIFIER) << "Check still running";
        return;
    }

    qCDebug(NOTIFIER) << "Running releasechecker";

    m_checkerProcess = new QProcess(this);
    m_checkerProcess->setProcessChannelMode(QProcess::ForwardedErrorChannel);
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    // Force utf-8. In case the system has bogus encoding configured we'll still
    // be able to properly decode.
    env.insert(QStringLiteral("PYTHONIOENCODING"), QStringLiteral("utf-8"));
    if (m_dbus->useDevel()) {
        env.insert(QStringLiteral("USE_DEVEL"), QStringLiteral("1"));
    }
    m_checkerProcess->setProcessEnvironment(env);
    connect(m_checkerProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this, &DistroReleaseNotifier::checkReleaseUpgradeFinished);
    m_checkerProcess->start(QStringLiteral("/usr/bin/python3"), QStringList() << checkerFile);
}

void DistroReleaseNotifier::checkReleaseUpgradeFinished(int exitCode)
{
    m_hasChecked = true;

    auto process = m_checkerProcess;
    m_checkerProcess->deleteLater();
    m_checkerProcess = nullptr;

    const QByteArray checkerOutput = process->readAllStandardOutput();

    // Make sure clearly invalid output doesn't get run through qjson at all.
    if (exitCode != 0 || checkerOutput.isEmpty()) {
        if (exitCode != 32) { // 32 is special exit on no new release
            qCWarning(NOTIFIER()) << "Failed to run releasechecker";
        } else {
            qCDebug(NOTIFIER()) << "No new release found";
        }
        return;
    }

    qCDebug(NOTIFIER) << checkerOutput;
    auto document = QJsonDocument::fromJson(checkerOutput);
    Q_ASSERT(document.isObject());
    auto map = document.toVariant().toMap();
    auto flavor = map.value(QStringLiteral("flavor")).toString();
    m_version = map.value(QStringLiteral("new_dist_version")).toString();
    m_name = NAME_FROM_FLAVOR ? flavor : KOSRelease().name();

    replyFinished();
}

void DistroReleaseNotifier::replyFinished()
{
    if (qEnvironmentVariableIsSet("MOCK_RELEASE")) {
        QDate eol;
        if (qEnvironmentVariableIsSet("MOCK_EOL")) {
            eol = QDate::currentDate().addDays(-1);
        } else {
            eol = QDate::currentDate().addDays(3);
        }

        m_notifier->show(m_name, QLatin1String("XX.YY"), eol);
        return;
    }

    // Requires a hard dependency on distro-info-data
    QFile file(QString::fromUtf8("/usr/share/distro-info/ubuntu.csv"));
    if (!file.open(QIODevice::ReadOnly)) {
        qCWarning(NOTIFIER) << "ubuntu.csv from distro-info-data not found";
        m_notifier->show(m_name, m_version, QDate());
    }

    QTextStream in(&file);
    bool firstLine = true;

    // We are given the release to upgrade to, not the current release's EOL
    // status, so track the eolString for n-1
    QString lastEOLString;

    while (!in.atEnd()) {
        if (firstLine) {
            firstLine = false;
            continue;
        }

        QStringList line = in.readLine().split(QLatin1Char(','));
        QString versionString = line[0].trimmed();
        const auto version = QVersionNumber::fromString(versionString);
        const auto eolString = line[5].trimmed();
        bool lts = false;

        // An LTS string with MetaRelease is formatted XX.YY.Z LTS
        // ubuntu.csv only has XX.YY LTS, so remove the point release if it exists
        if (version.segmentCount() >= 3) {
            const auto majorVersion = QString::number(version.majorVersion());
            const auto minorVersion = QString::number(version.minorVersion());
            versionString = majorVersion + QLatin1Char('.') + minorVersion + QLatin1String(" LTS");
            lts = true;
        }

        if (m_version != versionString) {
            lastEOLString = eolString;
            continue;
        }

        QDate eol = QDate::fromString(lastEOLString, QLatin1String("yyyy-MM-dd"));

        // This is specific to Ubuntu flavors, where the support cycle is 3 years
        if (lts) {
            eol = eol.addYears(-2);
         }

        m_notifier->show(m_name, versionString, eol);

        break;
    }

    return;
}

void DistroReleaseNotifier::releaseUpgradeActivated()
{
    if (m_pendingUpgrader) {
        // There's a time window between the user clicking upgrade and
        // the UI registering on dbus. We don't know what's the state of
        // things and consider the process pending. Should it fail we'll
        // display the error via UpgraderProcess.
        qCDebug(NOTIFIER) << "Upgrader requested but still waiting for one";
        return;
    }

    m_pendingUpgrader = new UpgraderProcess;
    m_pendingUpgrader->setUseDevel(m_dbus->useDevel());
    connect(m_pendingUpgrader, &UpgraderProcess::notPending,
            this, [this]() { m_pendingUpgrader = nullptr; });
    m_pendingUpgrader->run(); // returns once we are sure the process is up and running
}

void DistroReleaseNotifier::forceCheck()
{
    m_hasChecked = false;
    releaseUpgradeCheck();
}
